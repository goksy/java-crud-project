package sample;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DatabaseController {

    @FXML
    private TextField searchTextField;
    @FXML
    private ToggleGroup toggleGroup;
    @FXML
    private RadioButton insertIntoRadioBtn;
    @FXML
    private  RadioButton updateRadioBtn;
    @FXML
    private RadioButton deleteRadioBtn;
    @FXML
    private  TableView<Phone> tableView;

    @FXML
    private TableColumn<Phone, String> phoneIdColumn;
    @FXML
    private TableColumn<Phone, String> userIdColumn;
    @FXML
    private TableColumn<Phone, String> typeColumn;
    @FXML
    private TableColumn<Phone, String> breedColumn;

    @FXML
    private TextField phoneIdTextField;
    @FXML
    private TextField userIdTextField;
    @FXML
    private TextField typeTextField;
    @FXML
    private TextField breedTextField;

    @FXML
    private Button insertIntoBtn;
    @FXML
    private Button updateBtn;
    @FXML
    private Button deleteBtn;

    @FXML
    private Text errorMessage;

    // from Login Controller , the PK of the user that is logged in
    private  String userId;

    private static Connection connection;
    private static PreparedStatement preparedStatement;
    private static ResultSet resultSet;




    public void setFK(String userId) {
        // from Login Controller, the PK of the user that is logged in
        this.userId = userId;
    }

    @FXML
    public void insertBtnAction(ActionEvent actionEvent) {
        if(typeTextField.getText() !=null && !typeTextField.getText().isEmpty()
        && breedTextField.getText() != null && !breedTextField.getText().isEmpty()){

            Phone phone = new Phone(null, null, null, null);
            phone.setType(typeTextField.getText());
            phone.setBreed(breedTextField.getText());

            try{
                PreparedStatement preparedStatement =
                        DatabaseConnector.connect().prepareStatement("INSERT INTO phones VALUES(?, ?, ?, ?)");
                preparedStatement.setString(1, null);
                preparedStatement.setString(2, userId);
                preparedStatement.setString(3, phone.getType());
                preparedStatement.setString(4, phone.getBreed());
                preparedStatement.executeUpdate();

                DatabaseConnector.disconnect();
                initialize();

                errorMessage.setText("");



            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }

        }else{
            errorMessage.setText("Please complete all fields");
        }
    }

    /***
     * Sets up tableView and fills the table with data from the database.
     */
@FXML
    private  void initialize() {
        // initialize the tableView with four columns
        phoneIdColumn.setCellValueFactory(cellData -> cellData.getValue().phoneIdProperty());
        userIdColumn.setCellValueFactory(cellData -> cellData.getValue().userIdProperty());
        typeColumn.setCellValueFactory(cellData -> cellData.getValue().typeProperty());
        breedColumn.setCellValueFactory(cellData -> cellData.getValue().breedProperty());

        setVisibleItems();
        Platform.runLater(()-> {
            //fill the tableview with data from observableList
             System.out.println("User ID is: "+ userId);
            try{

                tableView.setItems(getPhoneData());
                buildPhone();
                sortFilterTableView();
                observeRadioButtonChanges();
            } catch (SQLException throwables){
                throwables.printStackTrace();
            }
        });
    }

    private void observeRadioButtonChanges() {
        toggleGroup.selectedToggleProperty().addListener((observableValue, oldValue, newValue) -> {

            // Cast object to Radio Button
            RadioButton radioButton = (RadioButton)newValue.getToggleGroup().getSelectedToggle();
            if(radioButton.getText().contains("Insert Into")){

                insertIntoBtn.setVisible(true);
                insertIntoBtn.setManaged(true);

                updateBtn.setVisible(false);
                updateBtn.setManaged(false);

                deleteBtn.setVisible(false);
                deleteBtn.setManaged(false);

                typeTextField.setVisible(true);
                typeTextField.setManaged(true);

                breedTextField.setVisible(true);
                breedTextField.setManaged(true);
            } else if (radioButton.getText().contains("Update")){

                insertIntoBtn.setVisible(false);
                insertIntoBtn.setManaged(false);

                updateBtn.setVisible(true);
                updateBtn.setManaged(true);

                deleteBtn.setVisible(false);
                deleteBtn.setManaged(false);

                typeTextField.setVisible(true);
                typeTextField.setManaged(true);

                breedTextField.setVisible(true);
                breedTextField.setManaged(true);
            } else {
                insertIntoBtn.setVisible(false);
                insertIntoBtn.setManaged(false);

                updateBtn.setVisible(false);
                updateBtn.setManaged(false);

                deleteBtn.setVisible(true);
                deleteBtn.setManaged(true);

                typeTextField.setVisible(false);
                typeTextField.setManaged(false);

                breedTextField.setVisible(false);
                breedTextField.setManaged(false);
            }
        });
    }

    private void sortFilterTableView() throws SQLException {

        FilteredList<Phone> filteredList =new FilteredList<>(getPhoneData(), p -> true);
        searchTextField.textProperty().addListener((observable, oldValue, newValue) ->{
            filteredList.setPredicate(phone -> {
                // if search text is empty display all pets
                if(newValue == null || newValue.isEmpty()){
                    return  true;
                }
                String lowerCaseFiler = newValue.toLowerCase();
                if(phone.getType().toLowerCase().contains(lowerCaseFiler)){
                    return true; // search string is a match

                }else if (phone.getBreed().toLowerCase().contains(lowerCaseFiler)){
                    return true; // search string is a match
                }
                return  false; // does not match
            });
        });
        SortedList<Phone> sortedList = new SortedList<>(filteredList);
        sortedList.comparatorProperty().bind(tableView.comparatorProperty());
        tableView.setItems(sortedList);

    }

    private void setVisibleItems() {
        Toggle toggle = toggleGroup.getSelectedToggle();
        System.out.println("Radio button selected: "+toggle);

        if(toggle == null){
            // set what the user see`s and does not see
            insertIntoBtn.setVisible(true);
            insertIntoBtn.setManaged(true);

            updateBtn.setVisible(false);
            updateBtn.setManaged(false);

            deleteBtn.setVisible(false);
            deleteBtn.setManaged(false);

            insertIntoRadioBtn.setSelected(true);
        }
    }

    /***
     * build a phone object by clicking on a table view row.
     * Populate the form fields using the phone object attributes
     */
    private void buildPhone() {
        tableView.setOnMouseClicked((MouseEvent mouseEvent) -> {
            if (mouseEvent.getClickCount() > 0){

                if (tableView.getSelectionModel().getSelectedItem() != null){
                    Phone phone = tableView.getSelectionModel().getSelectedItem();
                    phoneIdTextField.setText(phone.getPhoneId());
                    userIdTextField.setText(phone.getUserId());
                    typeTextField.setText(phone.getType());
                    breedTextField.setText(phone.getBreed());
                }
            }

        });
    }

    private ObservableList<Phone> getPhoneData() throws SQLException {

        connection = DatabaseConnector.connect();
        preparedStatement = connection.prepareStatement("SELECT * FROM phones WHERE user_id = ?");
        preparedStatement.setString(1, userId);
        resultSet = preparedStatement.executeQuery();

        ObservableList<Phone> phoneList = FXCollections.observableArrayList();

        while(resultSet.next()){
            String phoneId = resultSet.getString(1);
            String userId = resultSet.getString(2);
            String type = resultSet.getString(3);
            String breed = resultSet.getString(4);
            //construct Phone object
            Phone phone = new Phone(phoneId, userId, type, breed);
            // add the phone to the List
            phoneList.add(phone);
        }
        DatabaseConnector.disconnect();

        return phoneList;
    }
    @FXML
    public void updateBtnAction(ActionEvent actionEvent) {

        if(typeTextField.getText() != null && !typeTextField.getText().isEmpty()
        && breedTextField.getText() != null && !breedTextField.getText().isEmpty()){
            Phone phone = new Phone(null, null, null, null);
            phone.setPhoneId(phoneIdTextField.getText());
            phone.setUserId(userIdTextField.getText());
            phone.setType(typeTextField.getText());
            phone.setBreed(breedTextField.getText());
            try{
                PreparedStatement preparedStatement =
                        DatabaseConnector.connect().prepareStatement("UPDATE phones SET type = ?, breed = ? WHERE phone_id = ?");
                preparedStatement.setString(1, phone.getType());
                preparedStatement.setString(2, phone.getBreed());
                preparedStatement.setString(3, phone.getPhoneId());
                preparedStatement.executeUpdate();
                preparedStatement.close();

                DatabaseConnector.disconnect();
                initialize();

                errorMessage.setText("");
            } catch (SQLException throwables){
                throwables.printStackTrace();
            }

        } else{
            errorMessage.setText("Please complete all fields");
        }
    }
    @FXML
    public void deleteBtnAction(ActionEvent actionEvent) {
        if (typeTextField.getText() != null && !typeTextField.getText().isEmpty()) {
            Phone phone = new Phone(null, null, null, null);
            phone.setPhoneId(phoneIdTextField.getText());
            phone.setUserId(userIdTextField.getText());
            phone.setType(typeTextField.getText());
            phone.setBreed(breedTextField.getText());

            try {
                PreparedStatement preparedStatement =
                        DatabaseConnector.connect().prepareStatement("DELETE FROM phones WHERE phone_id = ?");

                preparedStatement.setString(1, phone.getPhoneId());
                preparedStatement.executeUpdate();
                preparedStatement.close();

                DatabaseConnector.disconnect();
                initialize();

                errorMessage.setText("");

                typeTextField.setText("");
                breedTextField.setText("");
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        } else {
            errorMessage.setText("Please select a phone from the table");
        }
    }
}
