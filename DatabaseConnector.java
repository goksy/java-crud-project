package sample;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnector {
    // public so AddressController can check if null or not
    public static Connection connection = null;

    public static Connection connect() {

    // Server address from user input
        String address = AddressController.address;

        try {
            connection = DriverManager.getConnection(
                    "jdbc:mariadb://" + address +
                    ":3306/phones_javafx?useLegacyDatetimeCode=false&serverTimezone=UTC",
                    "gokhan","4611751Go"
                    );
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return connection;

    }

    /***
     * Disconnect from the database
     */
    public static void disconnect() {

        if(connection != null){
            try {
                connection.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }
}
