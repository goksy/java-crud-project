package sample;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Phone {

    //Attributes or state
    private StringProperty phoneId;
    private StringProperty userId;
    private StringProperty type;
    private StringProperty breed;

    //Constructor
    public Phone(String phoneId, String userId, String type, String breed){
        this.phoneId = new SimpleStringProperty(phoneId);
        this.userId = new SimpleStringProperty(userId);
        this.type = new SimpleStringProperty(type);
        this.breed = new SimpleStringProperty(breed);
    }
    //Methods or Behaviour (Setter - Mutator, Getter -Accessor)
    public void  setPhoneId(String phoneId){
        this.phoneId.set(phoneId);
    }
    public  String getPhoneId(){
        return phoneId.get();
    }
    public StringProperty phoneIdProperty(){
        return  phoneId;
    }


    //Methods or Behavior (Setter - Mutator, Getter -Accessor)
    public void  setUserId(String userId){
        this.userId.set(userId);
    }
    public  String getUserId(){
        return userId.get();
    }
    public StringProperty userIdProperty(){
        return  userId;
    }

    //Methods or Behavior (Setter - Mutator, Getter -Accessor)
    public void  setType(String type){
        this.type.set(type);
    }
    public  String getType(){
        return type.get();
    }
    public StringProperty typeProperty(){
        return  type;
    }
    //Methods or Behavior (Setter - Mutator, Getter -Accessor)
    public void  setBreed(String breed){
        this.breed.set(breed);
    }
    public  String getBreed(){
        return breed.get();
    }
    public StringProperty breedProperty(){
        return  breed;
    }



}
